package com.mfaruk.kose.userservice.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "usertable")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User implements Serializable {

    private static final long serialVersionUID = -8538443883640195122L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 20, name = "FIRST_NAME")
    private String firstName;

    @Column(nullable = false, length = 20, name = "LAST_NAME")
    private String lastName;

    @Column(nullable = false, length = 50, name = "EMAIL", unique = true)
    private String email;

    @Column(nullable = false, name = "ENCRYPTED_PASSWORD")
    private String encryptedPassword;

    @Column(nullable = false, name = "USER_ID", unique = true)
    private String userId;

}
