package com.mfaruk.kose.userservice.model.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class UserResponse {

    private String firstName;
    private String lastName;
    private String email;
    private String userId;
}
