package com.mfaruk.kose.userservice.controller;

import com.mfaruk.kose.userservice.model.dto.UserDto;
import com.mfaruk.kose.userservice.model.request.UserRequest;
import com.mfaruk.kose.userservice.model.response.UserResponse;
import com.mfaruk.kose.userservice.service.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/status/check")
    public String status() {
        return "working";
    }

    @PostMapping
    public ResponseEntity<UserResponse> createUser(@Valid @RequestBody UserRequest userRequest) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UserDto userDto = userService.createUser(modelMapper.map(userRequest, UserDto.class));

        return ResponseEntity.status(HttpStatus.CREATED).body(modelMapper.map(userDto, UserResponse.class));
    }
}
