package com.mfaruk.kose.userservice.model.request;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class UserRequest {
    @NotNull(message = "firstName cannot be null")
    @Size(min = 2, message = "firstName must be more than 1 character")
    private String firstName;

    @NotNull(message = "lastName cannot be null")
    @Size(min = 2, message = "lastName must be more than 1 character")
    private String lastName;

    @NotNull(message = "password cannot be null")
    @Size(min = 4, max = 8, message = "password must be more than 3 characters and less than 8 characters")
    private String password;

    @NotNull(message = "email cannot be null")
    @Email
    private String email;
}
